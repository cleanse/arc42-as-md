#arc42 Template in MarkDown (MD)
This template is a port of the [arc42](http://arc42.org) template. You may use it for your projects with same restrictions as the original template.  
The idea is to have this running in full-featured services like [bitbucket](http://bitbucket.org).

##About arc42

###Basic Information arc42
**Version**: *6.1*  
**Authors**: *Dr. Peter Hruschka*, *Dr. Gernot Starke*  

###Basic Information arc42-as-md
**Updated**: *2014-07-03*  
**Website**: [arc42](http://arc42.org)(en), [arc42](http://arc42.de)(de)  
**Repository**: [bitbucket: arc42-as-md](https://bitbucket.org/cpelka/arc42-as-md)  
**More Information**: [Projects: arc42-as-markdown](http://delimiter.guru/projects)  
  
###What is arc42?
> arc42 supports software- and system architects. It is based upon practical experience of many architecture projects of different sizes and domains. It includes feedback of its many users.
> 
> arc42 contains a template for development, documentation and communication of software architectures. 
> 
> arc42 fits arbitrary technologies and tools.
> 
> arc42 therefore ensures better software- and system architectures.
> 
> Together with arc42 we propose a lightweight set of activities (process) for effective construction and development of software architectures.
> 
> You can use arc42 for free, even for commercial applications. We are happy when you quote us.
Source: [arc42](http://arc42.org)

