#3.2 Technical Context
***
**Navigation**: [Home](Home.md) | [3. Context](3-Context.md) | [3.1 Business Context](3.1-Business_Context.md) | 3.2 Technical Context | [3.3 External Interfaces](3.3-External_Interfaces.md)
***  

##*Page Information (TBR)*
> **Contents**  
> Specification of the communication channels linking your system to neighboring systems and the environment.
> 
> **Motivation**  
> Understanding of the media used for information exchange with neighboring systems, and the environment.
> 
> **Form**  
> E.g. UML deployment diagram describing channels to neighboring systems, together with a mapping table of logical input and output flows of the business context diagram (3.1) to the channels.

***
**Navigation**: [Home](Home.md) | [3. Context](3-Context.md) | [3.1 Business Context](3.1-Business_Context.md) | 3.2 Technical Context | [3.3 External Interfaces](3.3-External_Interfaces.md)
***  