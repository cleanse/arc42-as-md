#5. Building Block View
***
**Navigation**: [Home](Home.md) | [4. Solution Ideas and Strategy](4-Solution_Ideas_and_Strategy.md) | [6. Runtime View](6-Runtime_View.md)
***  

##Child Pages
5.1 [Level 1](5.1-Level_1.md)  
5.2 [Level 2](5.2-Level_2.md)  
5.3 [Level 3](5.3-Level_3.md)  
5.X [Template: Whitebox](5.x-Template_Whitebox.md)  
5.X [Template: Blackbox](5.x-Template_Blackbox.md)

##*Page Information (TBR)*
> **Contents**  
> Static decomposition of the system (introduced in the context diagram in section 3) into building blocks (modules, components, subsystems, subsidiary systems, classes, interfaces, packages, libraries, frameworks, layers, partitions, tiers, functions, macros, operations, data structures, …) and the relationships between them.
> 
> **Motivation**  
> This is the most important view, that must be part of each architecture documentation. It is the equivalent the the floor pan in building architecture.
> 
> **Form**
> The building block view is a hierarchical set of diagrams including the next lower level black boxes and their dependencies. Each diagram is described according to the white box template (see section 5.1).
> 
> 
> ![alt text](5-White-Blackbox-Hierarchy_en.jpg)
> [click for bigger size](5-White-Blackbox-Hierarchy_en.png)  
>  
> Level 1 contains the white box description of the overall system (system under development / SUD, introduced in the context diagram in section 3).
> 
> Level 2 zooms into the building blocks of Level 1 and is thus made up of the white box descriptions of all building blocks of Level 1.
> 
> Level 3 zooms into the building blocks of Level 2, etc.

***
**Navigation**: [Home](Home.md) | [4. Solution Ideas and Strategy](4-Solution_Ideas_and_Strategy.md) | [6. Runtime View](6-Runtime_View.md)
*** 