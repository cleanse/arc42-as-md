#2. Constraints
***
**Navigation**: [Home](Home.md) | [1. Introduction and Goals](1-Introduction_and_Goals.md) | 2. Constraints | [3. Context](3-Context.md)
***  

##Child Pages
2.1 [Technical Contraints](2.1-Technical_Constraints.md)  
2.2 [Organizational Constraints](2.2-Organizational_Constraints.md)  
2.3 [Conventions](2.3-Conventions.md)  

##*Page Information (TBR)*
> **Contents**  
> Any requirement that constrains software architects in their freedom of design decisions or the development process.
> 
> **Motivation**  
> Architects should know exactly where they are free in their design decisions and where they must adhere to constraints.
> 
> Constraints must always be dealt with; they may be negotiable, though.
> 
> **Form**  
> Informal lists, structured by the sub-sections of this section.
> 
> **Examples**  
> see sub-sections
> 
> **Background Information**  
> In the optimal case constraints are defined by requirements. In any case, at least the architects must be aware of constraints.
> 
> The influence of constraints is described by [Hofmeister et al] (Software Architecture, A Practical Guide, Addison Wesley 1999)  using the term "global analysis".

***
**Navigation**: [Home](Home.md) | [1. Introduction and Goals](1-Introduction_and_Goals.md) | [3. Context](3-Context.md)
***  