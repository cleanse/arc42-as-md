#8.10 Safety
***
**Navigation**: [Home](Home.md) | [8. Concepts](8-Concepts.md)
***

##*Page Information (TBR)*
> The safety of software systems deals with mechanisms that ensure that human life or our environment is not endangered.
> 
> Describe your concept here: identify those parts of the system that might endanger life and describe mechanism to ensure proper safety.

**Navigation**: [Home](Home.md) | [8. Concepts](8-Concepts.md) | [8.1 Domain Models](8.1-Domain_Models.md) | [8.2 Recurring or Generic Structures and Patterns](8.2-Generic_Structures_and_Patterns.md) | [8.3 Persistency](8.3-Persistency.md) | [8.4 User Interface](8.4-User_Interface.md) | [8.5 Ergonomics](8.5_Ergonomics.md) | [8.6 Flow of Control](8.6-Flow_of_Control.md) | [8.7 Transaction Processing](8.7-Transaction_Processing.md) | [8.8 Session Handling](8.8-Session_Handling.md) | [8.9 Security](8.9-Security.md) | 8.10 Safety | [8.11 Communication and Integration](8.11-Communication_and_Integration.md) | [8.12 Distribution](8.12-Distribution.md) | [8.13 Plausibility and Validity Checks](8.13-Plausibility_and_Validity_Checks.md) | [8.14 Exception/Error Handling](8.14-Error_Handling.md) | [8.15 System Management and Administration](8.15-System_Administration.md) | [8.16 Logging, Tracing](8.16-Logging_Tracing.md) | [8.17 Business Rules](8.17-Business_Rules.md) | [8.18 Configurability](8.18-Configurability.md) | [8.19 Parallelization and Threading](8.19-Parallelization_Threading.md) | [8.20 Internationalization](8.20-Internationalization.md) | [8.21 Migration](8.21-Migration.md) | [8.22 Testability](8.22-Testability.md) | [8.23 Scaling, Clustering](8.23-Scaling_Clustering.md) | [8.24 High Availability](8.24-High_Availability.md) | [8.25 Code Generation](8.25-Code_Generation.md) | [8.26 Build Management](8.26-Build_Management.md) 
***  