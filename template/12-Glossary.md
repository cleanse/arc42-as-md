#12. Glossary
***
**Navigation**: [Home](Home.md) | [11. Technical Risks](11-Technical_Risks.md) | 12. Glossary
*** 

##*Page Information (TBR)*
> **Contents**  
> The most important terms of the software architecture in alphabetic order.
> 
> **Motivation**  
> It should not be necessary to explain the usefulness of a glossary ...
> 
> **Form**  
> A simple table with columns <*Term*> and <*Definition*>

***
**Navigation**: [Home](Home.md) | [11. Technical Risks](11-Technical_Risks.md) | 12. Glossary
*** 