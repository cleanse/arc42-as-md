#8. Concepts
***
**Navigation**: [Home](Home.md) | [7. Deployment View](7-Deployment_View.md) | 8. Concepts | [9. Design Decisions](9-Design_Decisions.md)
***  

##Child Pages
[8.1 Domain Models](8.1-Domain_Models.md)  
[8.2 Recurring or Generic Structures and Patterns](8.2-Generic_Structures_and_Patterns.md)  
[8.3 Persistency](8.3-Persistency.md)  
[8.4 User Interface](8.4-User_Interface.md)  
[8.5 Ergonomics](8.5_Ergonomics.md)  
[8.6 Flow of Control](8.6-Flow_of_Control.md)  
[8.7 Transaction Processing](8.7-Transaction_Processing.md)  
[8.8 Session Handling](8.8-Session_Handling.md)  
[8.9 Security](8.9-Security.md)  
[8.10 Safety](8.10-Safety.md)  
[8.11 Communication and Integration](8.11-Communication_and_Integration.md)  
[8.12 Distribution](8.12-Distribution.md)  
[8.13 Plausibility and Validity Checks](8.13-Plausibility_and_Validity_Checks.md)  
[8.14 Exception/Error Handling](8.14-Error_Handling.md)  
[8.15 System Management and Administration](8.15-System_Administration.md)  
[8.16 Logging, Tracing](8.16-Logging_Tracing.md)  
[8.17 Business Rules](8.17-Business_Rules.md)  
[8.18 Configurability](8.18-Configurability.md)  
[8.19 Parallelization and Threading](8.19-Parallelization_Threading.md)  
[8.20 Internationalization](8.20-Internationalization.md)  
[8.21 Migration](8.21-Migration.md)  
[8.22 Testability](8.22-Testability.md)  
[8.23 Scaling, Clustering](8.23-Scaling_Clustering.md)  
[8.24 High Availability](8.24-High_Availability.md)  
[8.25 Code Generation](8.25-Code_Generation.md)  
[8.26 Build Management](8.26-Build_Management.md)    

##*Page Information (TBR)*
> **Contents**  
> The following chapters cover examples of frequent cross-cutting concerns (a.k.a. aspects in some programming languages)
> 
> Fill in these chapters if there is NO building block that covers this aspect. If some of the concepts are not relevant for your project mention this fact instead of removing the section.
> 
> **Motivation**  
> Some concept cannot be “factored” into a separate building block of the architecture (e.g. the topic "security"). This section of the template is the location where you can describe all decision for such a cross cutting topic in one central place. Nevertheless, you have to make sure that all your building blocks conform to such decisions.
> 
> **Form**  
> .. can be varied. Some concepts are plain natural language text with a freely chosen structure, some others may include models/scenarios using notations that are also applied in architecture views.

***
**Navigation**: [Home](Home.md) | [7. Deployment View](7-Deployment_View.md) | 8. Concepts | [9. Design Decisions](9-Design_Decisions.md)
***  