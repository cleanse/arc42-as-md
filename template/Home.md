#arc42 Template in MarkDown (MD)
This template is a port of the [arc42](http://arc42.org) template. You may use it for your projects with same restrictions as the original template.  
The idea is to have this running in full-featured services like [bitbucket](http://bitbucket.org).

##About arc42

###Basic Information arc42
**Version**: *6.1*  
**Authors**: *Dr. Peter Hruschka*, *Dr. Gernot Starke*  

###Basic Information arc42-as-md
**Updated**: *2014-07-03*  
**Website**: [arc42](http://arc42.org)(en), [arc42](http://arc42.de)(de)  
**Repository**: [bitbucket: arc42-as-md](https://bitbucket.org/cpelka/arc42-as-md)  
**More Information**: [arc42-as-markdown](http://delimiter.guru/arc42-as-markdown)  
  
###What is arc42?
> arc42 supports software- and system architects. It is based upon practical experience of many architecture projects of different sizes and domains. It includes feedback of its many users.
> 
> arc42 contains a template for development, documentation and communication of software architectures. 
> 
> arc42 fits arbitrary technologies and tools.
> 
> arc42 therefore ensures better software- and system architectures.
> 
> Together with arc42 we propose a lightweight set of activities (process) for effective construction and development of software architectures.
> 
> You can use arc42 for free, even for commercial applications. We are happy when you quote us.
Source: [arc42](http://arc42.org)


##Child Pages
1. [Introduction and Goals](1-Introduction_and_Goals.md)
2. [Contraints](2-Constraints.md)
3. [Context](3-Context.md)
4. [Solution Ideas and Strategy](4-Solution_Ideas_and_Strategy.md)
5. [Building Block View](5-Building_Block_View.md)
6. [Runtime View](6-Runtime_View.md)
7. [Deployment View](7-Deployment_View.md)
8. [Concepts](8-Concepts.md)
9. [Design Decisions](9-Design_Decisions.md)
10. [Quality Scenarios](10-Quality_Scenarios.md)
11. [Risks](11-Risks.md)
12. [Glossary](12-Glossary.md)
