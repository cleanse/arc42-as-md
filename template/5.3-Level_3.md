#5.3 Level 3
***
**Navigation**: [Home](Home.md) | [5. Building Block View](5-Building_Block_View.md) | [5.1 Level 1](5.1-Level_1.md) | [5.2 Level 2](5.2-Level_2.md) | 5.3 Level 3 | [Template: Whitebox](5.x-Template_Whitebox.md) | [Template: Blackbox](5.x_Template-Blackbox.md)
***  

##*Page Information (TBR)*
> **Content**
> Describe all building blocks from level 2 that need more refinement as a series of white box templates. The structure is identical to the structure of level 2. Duplicate the corresponding sub-sections as needed.
> 
> Simply use this section structure for any additional levels you would like to describe.
  
***
**Navigation**: [Home](Home.md) | [5. Building Block View](5-Building_Block_View.md) | [5.1 Level 1](5.1-Level_1.md) | [5.2 Level 2](5.2-Level_2.md) | 5.3 Level 3 | [Template: Whitebox](5.x-Template_Whitebox.md) | [Template: Blackbox](5.x-Template_Blackbox.md)
***  