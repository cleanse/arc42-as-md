#6. Runtime View
***
**Navigation**: [Home](Home.md) | [5. Building Block View](5-Building_Block_View.md) | 6. Runtime View | [7. Deployment View](7-Deployment_View.md)
***  

##Child Pages
6.1 [Runtime Scenario 1](6.1-Runtime_Scenario_1.md)  
6.2 Runtime Scenario 2  
6.3 Runtime Scenario 3  
6.n Runtime Scenario n

##*Page Information (TBR)*
> **Contents**  
> Alternative terms:
> 
> * Dynamic view 
> * Process view 
> * Workflow view 
> This view describes the behavior and interaction of the system’s building blocks as runtime elements (processes, tasks, activities, threads, …).
> 
> Select interesting runtime scenarios such as:
> 
> * How are the most important use cases executed by the architectural building blocks?
> * Which instances of architectural building blocks are created at runtime and how are they started, controlled, and stopped.
> * How do the system’s components co-operate with external and pre-existing components?
> * How is the system started (covering e.g. required start scripts, dependencies on external systems, databases, communications systems, etc.)?
>  
> 
> Note: The main criterion for the choice of possible scenarios (sequences, workflows) is their architectural relevancy. It is not important to describe a large number of scenarios. You should rather document a representative selection.
> 
> Candidates are: 
> 
> 1. The top 3 – 5 use cases 
> 2. System startup 
> 3. The system’s behavior on its most important external interfaces 
> 4. The system’s behavior in the most important error situations 
> 
> **Motivation**  
> Esp. for object-oriented architectures it is not sufficient to specify the building blocks with their interfaces, but also how instances of building blocks interact during runtime.
> 
> This view explains how the blocks interact, how they provide their functionality, how the systems works.
> 
> **Form**  
> Document the chosen scenarios using UML sequence diagrams, activity diagrams or communications diagrams. Enumerated lists are sometimes feasible.
> 
> Using object diagrams you can depict snapshots of existing runtime objects as well as instantiated relationships. The UML allows to distinguish between active and passive objects.

***
**Navigation**: [Home](Home.md) | [5. Building Block View](5-Building_Block_View.md) | 6. Runtime View | [7. Deployment View](7-Deployment_View.md)
***  