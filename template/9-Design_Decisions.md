#9. Design Decisions
***
**Navigation**: [Home](Home.md) | [8. Concepts](8-Concepts.md) | 9. Design Decisions | [10. Quality Scenarios](10-Quality_Scenarios.md)
***  

##Table of Contents
9.1 <*Decision Topic 1*>  
9.2 <*Decision Topic 2*>  
9.n <*Decision Topic n*>  
9.x [Template: Design Decision](9.x-Template_Design_Decision.md)  

##9.1 <*Decision Topic 1*>
<*describe design decision or insert/fill in template*> [Template: Design Decision](9.x-Template_Design_Decision.md)  
  
##9.2 <*Decision Topic 2*>
<*describe design decision or insert/fill in template*> [Template: Design Decision](9.x-Template_Design_Decision.md)  
  
##9.n <*Decision Topic n*>
<*describe design decision or insert/fill in template*> [Template: Design Decision](9.x-Template_Design_Decision.md)  

##*Page Information (TBR)*
> **Contents**  
> Document all important design decisions and their reasons!
> 
> **Motivation**  
> It is advantageous if all important design decisions can be found in one place. It is up to you to decide if a decision should be documented here or rather locally (e.g. in the white box descriptions of building blocks). In any case avoid redundancies.
> 
> **Form**  
> Informal list, if possible ordered by the decisions’ importance for the reader.
> 
> Alternatively, use the following template to structure your decisions: [Template: Design Decision](9.x-Template_Design_Decision.md)

***
**Navigation**: [Home](Home.md) | [8. Concepts](8-Concepts.md) | 9. Design Decisions | [10. Quality Scenarios](10-Quality_Scenarios.md)
***  