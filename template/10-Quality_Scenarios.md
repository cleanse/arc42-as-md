#10. Quality Scenarios
***
**Navigation**: [Home](Home.md) | [9. Design Decisions](9-Design_Decisions.md) | 10. Quality Scenarios | [11. Technical Risks](11-Technical_Risks.md)
***  

##Child Pages
10.1 [Quality Tree](10.1-Quality_Tree.md)
10.2 [Evaluation Scenarios](10.2-Evaluation_Scenarios.md)

##*Page Information (TBR)*
> This chapter summarizes all you (or other stakeholders) might need to systematically evaluate the architecture against the quality requirements.
> 
> It contains:
> 
> * Quality Tree (sometimes called utility tree), an overview of the quality requirements
> * Evaluation or quality scenarios - detailed descriptions of the quality requirements or goals.

***
**Navigation**: [Home](Home.md) | [9. Design Decisions](9-Design_Decisions.md) | 10. Quality Scenarios | [11. Technical Risks](11-Technical_Risks.md)
***  