#11. Technical Risks
***
**Navigation**: [Home](Home.md) | [10. Quality Scenarios](10-Quality_Scenarios.md) | 11. Technical Risks | [12. Glossary](12-Glossary.md)
***  

##*Page Information (TBR)*
> **Contents**  
> A list of identified technical risks, ordered by priority
> 
> **Motivation**  
> "Risk management is project management for grown-ups" (Tim Lister, Atlantic Systems Guild.) This should be your motto for systematic detection and evaluation of technical risks in the architecture, which will be needed by project management as part of the overall risk analysis.
> 
> **Form**  
> List of risks with probability of occurrence, amount of damage, options for risk avoidance or risk mitigation, ...

***
**Navigation**: [Home](Home.md) | [10. Quality Scenarios](10-Quality_Scenarios.md) | 11. Technical Risks | [12. Glossary](12-Glossary.md)
***  