#3. Context
***
**Navigation**: [Home](Home.md) | [2. Contraints](2-Constraints.md) | 3. Context | [4. Solution Ideas and Strategy](4-Solution_Ideas_and_Strategy.md)
***   

##Child Pages
3.1 [Business Context](3.1-Business_Context.md)  
3.2 [Technical Context](3.2-Technical_Context.md)  
3.3 [External Interfaces](3.3-External_Interfaces.md)  

##*Page Information (TBR)*
> **Contents**  
> The context view defines the boundaries of the system under development to distinguish it from neighboring systems. It thereby identifies the system’s relevant external interfaces.
> 
> Make sure that the interfaces are specified with all their relevant aspects (what is communicated, in which format is it communicated, what is the transport medium, …), even though some popular diagrams (such as the UML use case diagram) represent only a few aspects of the interface.
> 
> **Motivation**  
> The interfaces to neighboring systems are among most critical and risky aspects of a project. Ensure early on that you have understood them in their entirety.
> 
> **Form**  
> * Various context diagram (see below)
> * Lists of neighboring systems and their interfaces.

***
**Navigation**: [Home](Home.md) | [2. Contraints](2-Constraints.md) | 3. Context | [4. Solution Ideas and Strategy](4-Solution_Ideas_and_Strategy.md)
***  