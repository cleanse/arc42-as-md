#1. Introduction and Goals
***
**Navigation**: [Home](Home.md) | [2. Contraints](2-Constraints.md)
***

##Child Pages
1.1 [Requirements Overview](1.1-Requirements_Overview.md)  
1.2 [Quality Goals](1.2-Quality_Goals.md)  
1.3 [Stakeholders](1.3-Stakeholders.md)  

##*Page Information (TBR)*
> The introduction to the architecture documentation should list the driving forces that software architects must consider in their decisions.
>
> This includes on the one hand the fulfillment of functional requirements of the stakeholders, on the other hand the fulfillment of or compliance with required  constraints, always in consideration of the architecture goals.

***
**Navigation**: [Home](Home.md) | [2. Contraints](2-Constraints.md)
***